const deepstream = require('deepstream.io-client-js');

const ds = deepstream('wss://stream.fathomsys.com');

let FATHOM_KEY_DOC = {
  apiKey: ''
};

let topic_params = {
    venue: '',
    endpoint: 'devices/positions'
};

ds.login(FATHOM_KEY_DOC, (success, data) => {
    if (success) {
        console.log(data);
        let topic = `topic/${data.organizationId}/${topic_params.venue}/${topic_params.endpoint}`;
        console.log(topic);
        ds.event.subscribe(topic, (subject) => {
            console.log(JSON.stringify(subject));
        });
    } 
    else {
        console.log('ds login failed');
    }
});
