# se_ref_node


This is a node example client that logs into deepstream and prints the position events to the console.
It uses the deepstream.io-client-js package from deepstream.

## To Run Example
1) Install npm. 
See: https://www.npmjs.com/get-npm

2) Once npm is install navigate to directory and install dependencies:
```
	cd se_ref_node
	npm install
```
3) Open the node-example-client.js file and provide values for the variables _FATHOM_KEY_DOC.apiKey_ and _topic_params.venue_

4) Run code by:
```
	npm run start
```